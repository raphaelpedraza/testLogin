import { Font } from 'expo';
import React from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableOpacity, TextInput, Alert, ScrollView } from 'react-native';
import validate from 'validate.js';

export default class App extends React.Component {
	state = {
		email: null,
		emailError: '',
		password: null,
		passwordError: '',
		inputsValid: false,
		fontLoaded: false
	}
	
	async componentDidMount() {
		await Font.loadAsync({
		  'Arial': require('./assets/fonts/Arial.ttf')
		});
		this.setState({ fontLoaded: true });
	  }

	validateInput(field, value){
	  
		var {emailError, passwordError} = this.state;  
		  
		//Example: If field is email, then check if value follows the conditions of the email constraint.  
		var result = validate({[field]: value == '' ? null : value}, {[field]: constraints[[field]]}, {fullMessages: false} );
		
		//Get only the first error from result.
		var error =  result ? result[[field]][0]: null;
		console.log(error);
		
		//Evaluate existing errors and store to valid.
		var valid = emailError && passwordError;
		
		//Set states and update valid
		if(field == 'email'){
			this.setState({emailError: error});
			valid = ( passwordError==null && error==null );
		}
		else if(field == 'password'){
			this.setState({passwordError: error});
			valid = ( emailError==null && error==null );
		}
			
		//Set state	for enabling/disabling sign in button
		this.setState({inputsValid: valid});
		console.log("Inputs are valid: " + valid);
	} 

	onPressButton = () => {
		Alert.alert('','Login successful!')
	}		

	render() {
		if (!this.state.fontLoaded) return null;
		const {emailError, passwordError } = this.state;
		return (
		 
		  <ScrollView contentContainerStyle={{flexGrow: 1}}>
			<View style={{flex: 1, paddingHorizontal: 14,}}>
				<View style={styles.container}>
					<Image source={require('./assets/logo.png')} style={styles.logo} />
			
				</View>
				<View style={styles.container}>
					<Text style={styles.label}>Email</Text>
					<View style={styles.bordered}>
						<TextInput style={styles.input}
							onChangeText={(email) => { this.validateInput('email',email); }} 
							keyboardType='email-address'
							autoCapitalize='none'
							underlineColorAndroid='transparent'
							/>
					</View>	
					<Text style={styles.error}> {emailError ? emailError : null }</Text>
					
					<Text style={styles.label}>Password</Text>
					<View style={styles.bordered}>
						<TextInput style={styles.input}					
							onChangeText={(password) => { this.validateInput('password',password); } } 
							secureTextEntry={true}
							autoCapitalize='none'
							type="password" 
							underlineColorAndroid='transparent'
						/>
					</View>
					<Text style={styles.error}> {passwordError ? passwordError : null }</Text>
					
					<TouchableOpacity onPress={this.onPressButton} disabled={!this.state.inputsValid}>
						<View style={[styles.button,{opacity: this.state.inputsValid? 1 : 0.6}]}>
							<Text style={styles.buttonText}> Sign In </Text>
						</View>
					</TouchableOpacity>
					
				</View>
				
				
				
			</View>
		  </ScrollView>
		);
   }
}

const constraints = {
  email: {
    email: {
      message: "not correct format for email address"
    },presence: {
      message: "email cannot be blank."
    }
  },
  password: {
    length: {
      minimum: 6,
      maximum: 15,
      message: "please use at least 6-12 characters"
    },
    presence: {
      message: "password cannot be blank."
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  logo: {
	marginTop: 50, 
	alignSelf: 'center', 
	aspectRatio: 1.3, 
	resizeMode: 'contain'  
  },
  bordered: {
	  borderWidth: 1, 
	  borderColor: '#aa8fdb', 
	  borderRadius: 3,
  },
  button: {
	marginTop: 33,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#aa8fdb',
    borderRadius: 3,
  },
  buttonText: {
    padding: 10,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white'
  },
  input:{
	paddingLeft: 10,
	height: 43,
	fontFamily: 'Arial',
	fontSize: 16,
	color: '#393939',
  },
  error:{
	fontSize: 10,
	fontStyle: 'italic',
	color: 'red'  
  },
  label: {
	fontSize: 18,
	color: '#363636'  
  }
});
